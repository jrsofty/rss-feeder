package org.bitbucket.jrsofty.rss.models;

import java.io.Serializable;
import java.util.HashSet;

public class FeedGroup extends AbstractFeedObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9158242910720694677L;
    private HashSet<Feed> feeds = new HashSet<>();

    public HashSet<Feed> getFeeds() {
        return this.feeds;
    }

    public void setFeeds(final HashSet<Feed> feeds) {
        this.feeds = feeds;
    }

}
