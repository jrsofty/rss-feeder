package org.bitbucket.jrsofty.rss.models;

import java.io.Serializable;

public class FeedItem implements Serializable {

    private static final long serialVersionUID = 3907632430228130073L;
    private Long id;
    private String title;
    private String link;
    private String content;
    private String pubDate;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public String getPubDate() {
        return this.pubDate;
    }

    public void setPubDate(final String pubDate) {
        this.pubDate = pubDate;
    }

    @Override
    public int hashCode() {
        final int prime = 13;
        int code = 1;
        code = (prime * code) + (this.title == null ? 0 : this.title.hashCode());
        code = (prime * code) + (this.link == null ? 0 : this.link.hashCode());
        return code;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof FeedItem)) {
            return false;
        }
        final FeedItem other = (FeedItem) o;
        if (this.title.equals(other.getTitle()) && this.link.equals(other.getLink())) {
            return true;
        }
        return false;

    }

}
