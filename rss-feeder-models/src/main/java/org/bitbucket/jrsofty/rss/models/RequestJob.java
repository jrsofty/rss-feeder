package org.bitbucket.jrsofty.rss.models;

import java.util.Date;

public class RequestJob {
    private Long id;
    private String name;
    private String schedule;
    private String url;
    private Boolean enabled;
    private Date lastUpdate;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSchedule() {
        return this.schedule;
    }

    public void setSchedule(final String schedule) {
        this.schedule = schedule;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(final Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
