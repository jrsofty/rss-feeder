package org.bitbucket.jrsofty.rss.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Feed implements Serializable {

    private static final long serialVersionUID = 2845459884107712543L;
    private Long id;
    private String author;
    private String category;
    private String rights;
    private String title;
    private String description;
    private String generator;
    private String publishDate;
    private String link;
    private final ArrayList<FeedItem> items = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public String getRights() {
        return this.rights;
    }

    public void setRights(final String rights) {
        this.rights = rights;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getGenerator() {
        return this.generator;
    }

    public void setGenerator(final String generator) {
        this.generator = generator;
    }

    public String getPublishDate() {
        return this.publishDate;
    }

    public void setPublishDate(final String publishDate) {
        this.publishDate = publishDate;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public List<FeedItem> getItems() {
        return this.items;
    }

    public void setItems(final List<FeedItem> items) {
        this.items.clear();
        this.items.addAll(items);
    }
}
