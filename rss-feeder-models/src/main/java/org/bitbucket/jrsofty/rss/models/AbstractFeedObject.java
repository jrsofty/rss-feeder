package org.bitbucket.jrsofty.rss.models;

import java.io.Serializable;

public abstract class AbstractFeedObject implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6830592104874964619L;
    private Long id;
    private String name;
    private FeedGroup parent;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public FeedGroup getParent() {
        return this.parent;
    }

    public void setParent(final FeedGroup parent) {
        this.parent = parent;
    }

}
