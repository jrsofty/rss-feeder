package org.bitbucket.jrsofty.rss.business;

import java.util.Set;

import org.bitbucket.jrsofty.rss.models.Feed;
import org.bitbucket.jrsofty.rss.models.FeedItem;

public interface FeedBusiness {

    Feed createFeed(String uri);

    Feed getFeedById(Long id);

    Set<Feed> getFeedsByParentId(Long id);

    Feed updateFeedItems(Feed feed, Set<FeedItem> items);

    Feed modifyFeed(Feed feed);

    boolean deleteFeedById(Long id);

}
