package org.bitbucket.jrsofty.rss.rest.request;

public interface FeedRequestFactory {

    FeedRequest getFeedRequest(String url);

}
