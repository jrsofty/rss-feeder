package util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

public class ResourceLoader {

    public static String loadFeedData(final String path) throws IOException {

        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final InputStream stream = loader.getResourceAsStream(path);
        final byte[] content = IOUtils.toByteArray(stream);
        return new String(content, Charset.forName("UTF-8"));

    }

}
