package org.bitbucket.jrsofty.rss.service.scheduler;

public class RssSchedulerException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6254123247307090021L;

    public RssSchedulerException(final String message) {
        super(message);
    }

    public RssSchedulerException(final String message, final Throwable t) {
        super(message, t);
    }

}
