package org.bitbucket.jrsofty.rss.service.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

public class ScheduledRequest implements Job {

    private String url;
    private String name;

    public ScheduledRequest() {
    }

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        final JobKey key = context.getJobDetail().getKey();
        final JobDataMap dataMap = context.getMergedJobDataMap();

        // build RestClient Request

    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
