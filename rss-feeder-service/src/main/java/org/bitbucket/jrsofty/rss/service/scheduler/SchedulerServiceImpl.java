package org.bitbucket.jrsofty.rss.service.scheduler;

import java.util.HashMap;

import org.bitbucket.jrsofty.rss.models.RequestJob;
import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

class SchedulerServiceImpl implements SchedulerService {

    private final HashMap<String, Job> cachedJobs = new HashMap<>();
    private Scheduler scheduler = null;
    private boolean schedulerRunning = false;

    public SchedulerServiceImpl() throws RssSchedulerException {
        try {
            this.scheduler = StdSchedulerFactory.getDefaultScheduler();

        } catch (final Exception e) {
            throw new RssSchedulerException("Failed to get default scheduler.", e);
        }
    }

    @Override
    public void scheduleRequestJob(final RequestJob job) {

    }

    @Override
    public Job getScheduledJobByName(final String name) {
        if (!this.cachedJobs.containsKey(name)) {
            throw new RuntimeException("Unknown Scheduled Job");
        }

        return this.cachedJobs.get(name);
    }

    @Override
    public void startScheduler() throws RssSchedulerException {
        try {
            this.scheduler.start();
            this.schedulerRunning = true;
        } catch (final SchedulerException e) {
            throw new RssSchedulerException("Failed to start scheduler.", e);
        }

    }

    @Override
    public void stopScheduler() throws RssSchedulerException {
        try {
            this.scheduler.shutdown();
            this.schedulerRunning = false;
        } catch (final SchedulerException e) {
            throw new RssSchedulerException("Failed to start scheduler.", e);
        }

    }

    @Override
    public boolean isRunning() {
        return this.schedulerRunning;
    }

    private Job createJobForScheduling(final RequestJob job) {
        return null;
    }

}
