package org.bitbucket.jrsofty.rss.service.scheduler;

import org.bitbucket.jrsofty.rss.models.RequestJob;
import org.quartz.Job;

public interface SchedulerService {

    void startScheduler() throws RssSchedulerException;

    void stopScheduler() throws RssSchedulerException;

    void scheduleRequestJob(RequestJob job);

    Job getScheduledJobByName(String name);

    boolean isRunning();

    static class Factory {
        private static SchedulerService IMPL = null;

        static SchedulerService getInstance() throws RssSchedulerException {
            if (null == Factory.IMPL) {
                Factory.IMPL = new SchedulerServiceImpl();
            }
            return Factory.IMPL;
        }
    }

}
