package parser.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.bitbucket.jrsofty.rss.models.Feed;
import org.bitbucket.jrsofty.rss.parser.FeedParser;
import org.bitbucket.jrsofty.rss.parser.FeedParserException;
import org.bitbucket.jrsofty.rss.parser.FeedParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import util.ResourceLoader;

class DownloadExamples {

    HashMap<String, String> feeds = new HashMap<>();

    @BeforeEach
    void setUp() throws Exception {
        this.feeds.put("goblinsComic", "https://www.goblinscomic.org/comic/rss");
        // this.feeds.put("goonish", "https://egscomics.com/comic/rss");
        // this.feeds.put("girlGenius",
        // "https://www.girlgeniusonline.com/ggmain.rss");
        // this.feeds.put("questionableContent",
        // "https://www.questionablecontent.net/QCRSS.xml");
        // this.feeds.put("smbc", "https://www.smbc-comics.com/comic/rss");
        // this.feeds.put("swords", "https://swordscomic.com/comic/feed/");
        // this.feeds.put("xkcd", "https://xkcd.com/rss.xml");
    }

    // void test() throws IOException {
    // final ClientConfig config = new ClientConfig();
    // for (final Entry<String, String> entry : this.feeds.entrySet()) {
    // final Client client = ClientBuilder.newClient(config);
    // final WebTarget target = client.target(entry.getValue());
    // final String response = target.request().get(String.class).toString();
    // if (response != null) {
    // this.outputToFile(entry.getKey(), response);
    // }
    // }
    // Assertions.assertTrue(true);
    // }

    @Test
    void testGoblinsComic() throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData("rss-feeds/goblinsComic.xml");
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed feedObject = parser.parseFeed(feed);
        Assertions.assertTrue(feedObject != null);
    }

    private void outputToFile(final String key, final String content) throws IOException {
        final String filename = "dump/" + key + ".xml";
        Files.write(Paths.get(filename), content.getBytes());
    }

}
