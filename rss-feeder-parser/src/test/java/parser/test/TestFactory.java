package parser.test;

import java.io.IOException;

import org.bitbucket.jrsofty.rss.parser.FeedParser;
import org.bitbucket.jrsofty.rss.parser.FeedParserFactory;
import org.bitbucket.jrsofty.rss.parser.rss.RSSFeedParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import util.ResourceLoader;

class TestFactory {

    @Test
    public void testCreationOfFactoryImpl() {
        final FeedParserFactory factory = FeedParserFactory.getFactory();
        Assertions.assertNotNull(factory);
    }

    @Test
    public void testCreationOfRssFeedParser() throws IOException {
        final String feed = ResourceLoader.loadFeedData("rss-feeds/girl-genius.xml");
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);

        Assertions.assertTrue(parser instanceof RSSFeedParser);
    }

}
