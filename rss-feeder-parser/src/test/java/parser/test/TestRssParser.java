package parser.test;

import java.io.IOException;
import java.util.stream.Stream;

import org.bitbucket.jrsofty.rss.models.Feed;
import org.bitbucket.jrsofty.rss.parser.FeedParser;
import org.bitbucket.jrsofty.rss.parser.FeedParserException;
import org.bitbucket.jrsofty.rss.parser.FeedParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import util.ResourceLoader;

class TestRssParser {

    private static Logger LOG = LoggerFactory.getLogger(TestRssParser.class);

    @ParameterizedTest
    @ArgumentsSource(CustomFeedTitleArgumentProvider.class)
    public void testParseOfFeedTitle(final String feedPath, final String title) throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData(feedPath);
        final Long start = System.currentTimeMillis();
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed result = parser.parseFeed(feed);
        final Long time = System.currentTimeMillis() - start;
        TestRssParser.LOG.info(String.format("Feed %s took %s ms to parse", feedPath, time));
        Assertions.assertEquals(title, result.getTitle());
    }

    @ParameterizedTest
    @ArgumentsSource(CustomFeedDescriptionArgumentProvider.class)
    public void testParseOfFeedDescription(final String feedPath, final String title) throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData(feedPath);
        final Long start = System.currentTimeMillis();
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed result = parser.parseFeed(feed);
        final Long time = System.currentTimeMillis() - start;
        TestRssParser.LOG.info(String.format("Feed %s took %s ms to parse", feedPath, time));
        Assertions.assertEquals(title, result.getDescription());
    }

    @ParameterizedTest
    @ArgumentsSource(CustomFeedDescriptionArgumentProvider.class)
    public void testParseOfFeedLastBuild(final String feedPath, final String title) throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData(feedPath);
        final Long start = System.currentTimeMillis();
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed result = parser.parseFeed(feed);
        final Long time = System.currentTimeMillis() - start;
        TestRssParser.LOG.info(String.format("Feed %s took %s ms to parse", feedPath, time));
        Assertions.assertEquals(title, result.getDescription());
    }

    @ParameterizedTest
    @ArgumentsSource(CustomFeedItemCountArgumentProvider.class)
    public void testNumberOfItemsPerFeed(final String feedPath, final int count) throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData(feedPath);
        final Long start = System.currentTimeMillis();
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed result = parser.parseFeed(feed);
        final Long time = System.currentTimeMillis() - start;
        TestRssParser.LOG.info(String.format("Feed %s took %s ms to parse", feedPath, time));
        Assertions.assertEquals(count, result.getItems().size());
    }

    @ParameterizedTest
    @ArgumentsSource(CustomFeedLinkArgumentProvider.class)
    public void testFeedLink(final String feedPath, final String link) throws IOException, FeedParserException {
        final String feed = ResourceLoader.loadFeedData(feedPath);
        final Long start = System.currentTimeMillis();
        final FeedParser parser = FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
        final Feed result = parser.parseFeed(feed);
        final Long time = System.currentTimeMillis() - start;
        TestRssParser.LOG.info(String.format("Feed %s took %s ms to parse", feedPath, time));
        Assertions.assertEquals(link, result.getLink());
    }

    // @ParameterizedTest
    // @ArgumentsSource(FeedItemLinkArgumentProvider.class)
    // public void testItemLinks(final String feedPath, final String link)
    // throws IOException, FeedParserException {
    // final String feed = ResourceLoader.loadFeedData(feedPath);
    // final FeedParser parser =
    // FeedParserFactory.getFactory().createFeedParserFromFeed(feed);
    // final Feed result = parser.parseFeed(feed);
    // final FeedItem item = result.getItems().get(0);
    // Assertions.assertEquals(link, item.getLink());
    //
    // }

    static class FeedItemLinkArgumentProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {
            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml",
                            "http://www.girlgeniusonline.com/comic.php?date=20190612"),
                    Arguments.of("rss-feeds/feed.xml",
                            "http://www.alarminglybad.com/comic/hazing/"),
                    Arguments.of("rss-feeds/feedburner.xml",
                            "http://feedproxy.google.com/~r/satwcomic/~3/PEuMZ_OgO1E/secrets-secrets")
            // , Arguments.of("rss-feeds/tapas.io.xml", new String[] {}),
            // Arguments.of("rss-feeds/xkcd.xml", new String[] {}),
            // Arguments.of("rss-feeds/smbc.xml", new String[] {})

            );
        }

    }

    static class CustomFeedLinkArgumentProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {

            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml", "http://www.girlgeniusonline.com/"),
                    Arguments.of("rss-feeds/girlGenius.xml", "http://www.girlgeniusonline.com/"),
                    Arguments.of("rss-feeds/goblinsComic.xml", "https://www.goblinscomic.com/"),
                    Arguments.of("rss-feeds/goonish.xml", "https://www.egscomics.com/"),
                    Arguments.of("rss-feeds/questionableContent.xml", "http://www.questionablecontent.net"),
                    Arguments.of("rss-feeds/feed.xml", "http://www.alarminglybad.com"),
                    Arguments.of("rss-feeds/feedburner.xml", "https://satwcomic.com"),
                    Arguments.of("rss-feeds/tapas.io.xml", "https://tapas.io/series/6016"),
                    Arguments.of("rss-feeds/xkcd.xml", "https://xkcd.com/"),
                    Arguments.of("rss-feeds/smbc.xml", "https://www.smbc-comics.com/"),
                    Arguments.of("rss-feeds/swords.xml", "https://swordscomic.com/comic/")

            );

        }
    }

    static class CustomFeedItemCountArgumentProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {
            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml", 20),
                    Arguments.of("rss-feeds/girlGenius.xml", 20),
                    Arguments.of("rss-feeds/goblinsComic.xml", 20),
                    Arguments.of("rss-feeds/goonish.xml", 20),
                    Arguments.of("rss-feeds/questionableContent.xml", 103),
                    Arguments.of("rss-feeds/feed.xml", 10),
                    Arguments.of("rss-feeds/feedburner.xml", 30),
                    Arguments.of("rss-feeds/tapas.io.xml", 10),
                    Arguments.of("rss-feeds/xkcd.xml", 4),
                    Arguments.of("rss-feeds/smbc.xml", 20),
                    Arguments.of("rss-feeds/swords.xml", 10)

            );
        }

    }

    static class CustomFeedTitleArgumentProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {

            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml", "Girl Genius"),
                    Arguments.of("rss-feeds/girlGenius.xml", "Girl Genius"),
                    Arguments.of("rss-feeds/goblinsComic.xml", "Goblins"),
                    Arguments.of("rss-feeds/goonish.xml", "El Goonish Shive"),
                    Arguments.of("rss-feeds/questionableContent.xml", "QC RSS"),
                    Arguments.of("rss-feeds/feed.xml", "Alarmingly Bad"),
                    Arguments.of("rss-feeds/feedburner.xml", "Scandinavia and the World"),
                    Arguments.of("rss-feeds/tapas.io.xml", "Shen Comix"),
                    Arguments.of("rss-feeds/xkcd.xml", "xkcd.com"),
                    Arguments.of("rss-feeds/smbc.xml", "Saturday Morning Breakfast Cereal"),
                    Arguments.of("rss-feeds/swords.xml", "Swords")

            );
        }
    }

    static class CustomFeedDescriptionArgumentProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {

            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml", "Girl Genius Online Comic"),
                    Arguments.of("rss-feeds/girlGenius.xml", "Girl Genius Online Comic"),
                    Arguments.of("rss-feeds/goblinsComic.xml", "Latest Goblins comics and news"),
                    Arguments.of("rss-feeds/goonish.xml", "Latest El Goonish Shive comics and news"),
                    Arguments.of("rss-feeds/questionableContent.xml", "The Official QC RSS Feed"),
                    Arguments.of("rss-feeds/feed.xml", "Stupid. Funny. Updates Mondays."),
                    Arguments.of("rss-feeds/feedburner.xml", "The latest issues."),
                    Arguments.of("rss-feeds/tapas.io.xml", "I'm Shen, and there are my comix. Enjoy & thank you for reading!"),
                    Arguments.of("rss-feeds/xkcd.xml", "xkcd.com: A webcomic of romance and math humor."),
                    Arguments.of("rss-feeds/smbc.xml", "Latest Saturday Morning Breakfast Cereal comics and news"),
                    Arguments.of("rss-feeds/swords.xml", "Read Swords online!")

            );
        }
    }

    static class CustomFeedLastBuildArgumentProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) throws Exception {

            return Stream.of(
                    Arguments.of("rss-feeds/girl-genius.xml", "Thu, 13 Jun 2019 18:25:48 +0000"),
                    Arguments.of("rss-feeds/girlGenius.xml", "Fri, 26 Feb 2021 20:59:20 +0000"),
                    Arguments.of("rss-feeds/goblinsComic.xml", null),
                    Arguments.of("rss-feeds/goonish.xml", null),
                    Arguments.of("rss-feeds/questionableContent.xml", "Wed, 24 Feb 2021 22:21:52 -0400"),
                    Arguments.of("rss-feeds/feed.xml", "Mon, 03 Jun 2019 12:06:44 +0000"),
                    Arguments.of("rss-feeds/feedburner.xml", null),
                    Arguments.of("rss-feeds/tapas.io.xml", "Sat, 05 Jan 2019 01:16:14 GMT"),
                    Arguments.of("rss-feeds/xkcd.xml", null),
                    Arguments.of("rss-feeds/smbc.xml", null),
                    Arguments.of("rss-feeds/swords.xml", null)

            );
        }
    }

}
