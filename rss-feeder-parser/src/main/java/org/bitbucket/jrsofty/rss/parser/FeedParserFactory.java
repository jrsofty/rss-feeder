package org.bitbucket.jrsofty.rss.parser;

import org.bitbucket.jrsofty.rss.parser.atom.AtomFeedParser;
import org.bitbucket.jrsofty.rss.parser.rss.RSSFeedParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface FeedParserFactory {

    Logger LOG = LoggerFactory.getLogger(FeedParserFactory.class);

    static FeedParserFactory getFactory() {

        return new FeedParserFactory() {

            @Override
            public FeedParser createFeedParserFromFeed(final String feed) {
                FeedParser parser;
                if (this.isRss(feed)) {
                    FeedParserFactory.LOG.debug("RSS Feed Identified");
                    parser = new RSSFeedParser();
                } else if (this.isAtom(feed)) {
                    FeedParserFactory.LOG.debug("Atom Feed Identified");
                    parser = new AtomFeedParser();
                } else {
                    FeedParserFactory.LOG.error("Unable to identify the feed type");
                    throw new RuntimeException("Unidentified feed.");
                }

                return parser;
            }

            private boolean isRss(final String feed) {
                final String rssPattern = "<rss";
                return feed.contains(rssPattern);
            }

            private boolean isAtom(final String feed) {
                final String atomPattern = "<feed";
                return feed.contains(atomPattern);
            }

        };

    }

    FeedParser createFeedParserFromFeed(String feed);

}
