package org.bitbucket.jrsofty.rss.parser;

import org.bitbucket.jrsofty.rss.models.Feed;

public interface FeedParser {

    Feed parseFeed(String feed) throws FeedParserException;

}
