package org.bitbucket.jrsofty.rss.parser.rss;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.bitbucket.jrsofty.rss.models.Feed;
import org.bitbucket.jrsofty.rss.models.FeedItem;
import org.bitbucket.jrsofty.rss.parser.FeedParser;
import org.bitbucket.jrsofty.rss.parser.FeedParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RSSFeedParser implements FeedParser {

    private static Logger LOG = LoggerFactory.getLogger(RSSFeedParser.class);
    private static final String TITLE_PATH = "//rss/channel/title/text()";
    private static final String LINK_PATH = "//rss/channel/link/text()";
    private static final String DESC_PATH = "//rss/channel/description/text()";
    private static final String GEN_PATH = "//rss/channel/generator/text()";
    private static final String BUILD_PATH = "//rss/channel/lastBuildPath/text()";
    private static final String ITEMS_PATH = "//rss/channel/item";

    private final DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
    private final XPathFactory xFactory = XPathFactory.newDefaultInstance();
    private final XPath xpath;

    public RSSFeedParser() {
        this.factory.setNamespaceAware(true);
        this.xpath = this.xFactory.newXPath();
        RSSFeedParser.LOG.debug("Initialized RSS Feed Parser");
    }

    @Override
    public Feed parseFeed(final String feed) throws FeedParserException {
        final Document document;
        try {
            RSSFeedParser.LOG.debug("Loading feed document");
            document = this.loadDocument(feed);
            RSSFeedParser.LOG.debug("Feed document loaded");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new FeedParserException("Failure to load feed.", e);
        }

        final Feed feedObject = new Feed();

        this.parseFeed(document, feedObject);
        return feedObject;
    }

    private void parseFeed(final Document document, final Feed feedObject) throws FeedParserException {
        RSSFeedParser.LOG.debug("Begin feed parsing");
        try {
            RSSFeedParser.LOG.debug("Begin parsing");
            feedObject.setTitle(this.getTitle(document));
            RSSFeedParser.LOG.trace("Title set");
            feedObject.setDescription(this.getFeedDescription(document));
            RSSFeedParser.LOG.trace("Description Set");
            feedObject.setPublishDate(this.getLastBuild(document));
            RSSFeedParser.LOG.trace("publication date set");
            feedObject.getItems().addAll(this.getItems(document));
            feedObject.setLink(this.getFeedLink(document));
            RSSFeedParser.LOG.debug("Feed parsing complete.");
        } catch (final XPathExpressionException e) {
            throw new FeedParserException("Failure to parse feed", e);
        }
    }

    private String getFeedLink(final Document document) throws XPathExpressionException {
        final XPathExpression linkExpression = this.xpath.compile(RSSFeedParser.LINK_PATH);
        final String link = linkExpression.evaluate(document);
        return link;
    }

    private String getFeedDescription(final Document document) throws XPathExpressionException {
        final XPathExpression descExpression = this.xpath.compile(RSSFeedParser.DESC_PATH);
        final String description = descExpression.evaluate(document);
        return description;
    }

    private String getTitle(final Document document) throws XPathExpressionException {
        final XPathExpression titleExpression = this.xpath.compile(RSSFeedParser.TITLE_PATH);
        final String titleNode = titleExpression.evaluate(document);
        return titleNode;
    }

    private String getLastBuild(final Document document) throws XPathExpressionException {
        final XPathExpression express = this.xpath.compile(RSSFeedParser.BUILD_PATH);
        final String res = express.evaluate(document);
        return res;
    }

    private List<FeedItem> getItems(final Document document) throws XPathExpressionException {
        RSSFeedParser.LOG.debug("Begin item parsing");
        final XPathExpression expression = this.xpath.compile(RSSFeedParser.ITEMS_PATH);
        final NodeList nodes = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
        final ArrayList<FeedItem> items = new ArrayList<>();
        RSSFeedParser.LOG.debug(String.format("There are %s items to parse", nodes.getLength()));
        for (int i = 0; i < nodes.getLength(); i++) {
            RSSFeedParser.LOG.trace(String.format("Parsing item %s", i));
            final FeedItem item = this.createItem(nodes.item(i));
            items.add(item);
        }
        RSSFeedParser.LOG.debug("Items created");
        return items;
    }

    private FeedItem createItem(final Node xmlItem) throws XPathExpressionException {
        final FeedItem item = new FeedItem();
        item.setTitle(this.getItemTitle(xmlItem));
        item.setLink(this.getItemLink(xmlItem));
        item.setContent(this.getItemContent(xmlItem));
        item.setPubDate(this.getItemPubDate(xmlItem));
        return item;
    }

    private String getItemTitle(final Node xmlItem) throws XPathExpressionException {
        final XPathExpression expression = this.xpath.compile("title/text()");
        final String result = expression.evaluate(xmlItem);
        return result;
    }

    private String getItemLink(final Node xmlItem) throws XPathExpressionException {
        final XPathExpression expression = this.xpath.compile("link/text()");
        final String link = expression.evaluate(xmlItem);
        return link;
    }

    private String getItemContent(final Node xmlItem) throws XPathExpressionException {
        final XPathExpression expression = this.xpath.compile("description/text()");
        final String result = expression.evaluate(xmlItem);
        return result;
    }

    private String getItemPubDate(final Node xmlItem) throws XPathExpressionException {
        final XPathExpression expression = this.xpath.compile("pubDate/text()");
        final String result = expression.evaluate(xmlItem);
        return result;
    }

    private Document loadDocument(final String feed) throws ParserConfigurationException, SAXException, IOException {
        final DocumentBuilder builder = this.factory.newDocumentBuilder();
        final ByteArrayInputStream bais = new ByteArrayInputStream(feed.getBytes(Charset.forName("utf-8")));
        final Document document = builder.parse(bais);
        return document;
    }

}
