package org.bitbucket.jrsofty.rss.parser;

public class FeedParserException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -5859419511072539372L;

    public FeedParserException(final String message) {
        super(message);
    }

    public FeedParserException(final String message, final Throwable t) {
        super(message, t);
    }

    public FeedParserException(final Throwable t) {
        super(t);
    }

}
